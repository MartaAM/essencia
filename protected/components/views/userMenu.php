<ul>
	<li><?php echo CHtml::link('Crear Nuevo Post',array('post/create')); ?></li>
	<li><?php echo CHtml::link('Administrar Posts',array('post/admin')); ?></li>
	<li><?php echo CHtml::link('Aprobar Comentarios',array('comentario/index')) . ' (' . Comentario::model()->pendingComentarioCount . ')'; ?></li>
	<li><?php echo CHtml::link('Logout',array('site/logout')); ?></li>
</ul>