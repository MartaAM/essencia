<ul>
	<?php foreach($this->getRecentComentarios() as $comment): ?>
	<li><?php echo $comment->authorLink; ?> on
		<?php echo CHtml::link(CHtml::encode($comment->post->titulo), $comment->getUrl()); ?>
	</li>
	<?php endforeach; ?>
</ul>