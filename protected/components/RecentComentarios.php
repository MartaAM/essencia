<?php

Yii::import('zii.widgets.CPortlet');

class RecentComentarios extends CPortlet
{
	public $title='Comentarios Recientes';
	public $maxComentarios=10;

	public function getRecentComentarios()
	{
		return Comentario::model()->findRecentComentarios($this->maxComentarios);
	}

	protected function renderContent()
	{
		$this->render('recentComentarios');
	}
}