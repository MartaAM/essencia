<?php

// this contains the application parameters that can be maintained via GUI
return array(
	'postsPerPage'=>10,
	// maximum number of comments that can be displayed in recent comments portlet
	'recentComentarioCount'=>10,
	// maximum number of tags that can be displayed in tag cloud portlet
	'tagCloudCount'=>20,
	// whether post comments need to be approved before published
	'commentNeedApproval'=>true,
);
