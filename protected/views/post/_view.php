<div class="post">
	<div class="title">
		<?php echo CHtml::link(CHtml::encode($data->titulo), $data->url); ?>
	</div>
	<div class="author">
		posted by <?php echo $data->autor->nombre . ' on ' . date('F j, Y',$data->create_time); ?>
	</div>
	<div class="content">
		<?php
			$this->beginWidget('CMarkdown', array('purifyOutput'=>true));
			echo $data->contenido;
			$this->endWidget();
		?>
	</div>
	<div class="nav">
		<b>Tags:</b>
		<?php echo implode(', ', $data->tagLinks); ?>
		<br/>
		<?php echo CHtml::link('Permalink', $data->url); ?> |
		<?php echo CHtml::link("Comentarios ({$data->comentarioCount})",$data->url.'#comentarios'); ?> |
		Última actualización <?php echo date('F j, Y',$data->update_time); ?>
	</div>
</div>