<?php
/* @var $this PostController */
/* @var $model Post */
/* @var $form CActiveForm */
?>
<div class="form">

<?php $form=$this->beginWidget('CActiveForm'); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'titulo'); ?>
		<?php echo $form->textField($model,'titulo',array('size'=>80,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'titulo'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'contenido'); ?>
		<?php $this->widget('ext.redactor.ERedactorWidget', 
				array(
			        'model' => $model,
			        'attribute' => 'contenido',
			       	'htmlOptions'=>array(
		                'rows'=>20
		            ),
		            'options'=>array(
		                'cleanOnPaste'=>false,
		                'linebreaks'=>false,
		                'replaceDivs'=>false,
		                'allowedTags'=>array('span', 'button', 'i', 'div', 'p', 'h1', 'h2', 'h3', 'h4', 'h5', 'ul', 'li', 'ol', 'a', 'img', 'br', 'span'),
		                'fileUpload'=>Yii::app()->createUrl('post/fileUpload',array(
		                    'attr'=>'contenido',
		                )),
		                'fileUploadErrorCallback'=>new CJavaScriptExpression(
		                    'function(obj,json) { alert(json.error); }'
		                ),
		                'imageUpload'=>Yii::app()->createUrl('post/imageUpload',array(
		                    'attr'=>'contenido',
		                )),
		                'imageGetJson'=>Yii::app()->createUrl('post/imageList',array(
		                    'attr'=>'contenido',
		                )),
		                'imageUploadErrorCallback'=>new CJavaScriptExpression(
		                    'function(obj,json) { alert(json.error); }'
		                ),
		            ),
			    )
		    );
    	?>
		<?php echo $form->error($model,'contenido'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'status'); ?>
		<?php echo $form->dropDownList($model,'status',Lookup::items('PostStatus')); ?>
		<?php echo $form->error($model,'status'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'tags'); ?>
		<?php $this->widget('CAutoComplete', array(
			'model'=>$model,
			'attribute'=>'tags',
			'url'=>array('suggestTags'),
			'multiple'=>true,
			'htmlOptions'=>array('size'=>50),
		)); ?>
		<p class="hint">Please separate different tags with commas.</p>
		<?php echo $form->error($model,'tags'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->