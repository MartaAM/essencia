<?php foreach($comments as $comentario): ?>
<div class="comment" id="c<?php echo $comentario->id; ?>">

	<?php echo CHtml::link("#{$comentario->id}", $comentario->getUrl($post), array(
		'class'=>'cid',
		'title'=>'Permalink to this comment',
	)); ?>

	<div class="author">
		<?php echo $comentario->autorLink; ?> says:
	</div>

	<div class="time">
		<?php echo date('F j, Y \a\t h:i a',$comentario->fecha_creacion); ?>
	</div>

	<div class="content">
		<?php echo nl2br(CHtml::encode($comentario->contenido)); ?>
	</div>

</div><!-- comment -->
<?php endforeach; ?>