<?php
$this->breadcrumbs=array(
	$model->titulo,
);
$this->pageTitle=$model->titulo;
?>

<?php $this->renderPartial('_view', array(
	'data'=>$model,
)); ?>

<div id="comments">
	<?php if($model->comentarioCount>=1): ?>
		<h3>
			<?php echo $model->comentarioCount>1 ? $model->comentarioCount . ' comentarios' : '1 comentario'; ?>
		</h3>

		<?php $this->renderPartial('_comments',array(
			'post'=>$model,
			'comments'=>$model->comments,
		)); ?>
	<?php endif; ?>

	<h3>Deja tu comentario</h3>

	<?php if(Yii::app()->user->hasFlash('commentSubmitted')): ?>
		<div class="flash-success">
			<?php echo Yii::app()->user->getFlash('commentSubmitted'); ?>
		</div>
	<?php else: ?>
		<?php $this->renderPartial('/comentario/_form',array(
			'model'=>$comentario,
		)); ?>
	<?php endif; ?>

</div><!-- comments -->
