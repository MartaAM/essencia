<?php
/* @var $this ComunidadController */
/* @var $model Comunidad */

$this->breadcrumbs=array(
	'Comunidads'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Comunidad', 'url'=>array('index')),
	array('label'=>'Manage Comunidad', 'url'=>array('admin')),
);
?>

<h1>Create Comunidad</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>