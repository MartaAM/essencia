<?php
/* @var $this ComunidadController */
/* @var $model Comunidad */

$this->breadcrumbs=array(
	'Comunidads'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Comunidad', 'url'=>array('index')),
	array('label'=>'Create Comunidad', 'url'=>array('create')),
	array('label'=>'Update Comunidad', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Comunidad', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Comunidad', 'url'=>array('admin')),
);
?>

<h1>View Comunidad #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'nombre',
	),
)); ?>
