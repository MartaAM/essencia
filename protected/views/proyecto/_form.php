<?php
/* @var $this ProyectoController */
/* @var $model Proyecto */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'proyecto-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'nombre'); ?>
		<?php echo $form->textField($model,'nombre',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'nombre'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'descripcion'); ?>
		<?php echo $form->textArea($model,'descripcion',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'descripcion'); ?>
	</div>

	<div class="row bootstrap">
        <?php echo $form->label($model, 'gallery_id')?>

		<?php if($model->galleryBehavior->getGallery() === NULL): ?>

		    <p>Antes de añadir fotos a la galeria, necesita guardar primero el proyecto.</p>

		<?php else: ?>

		    <?php $this->widget('GalleryManager', array
		    (
		        'gallery' => $model->galleryBehavior->getGallery(),
                'controllerRoute'=>'/gallery'
		    )); ?>

		<?php endif; ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'url'); ?>
		<?php echo $form->textField($model,'url',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'url'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'tecnologia'); ?>
		<?php echo $form->textArea($model,'tecnologia',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'tecnologia'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'tipologia_id'); ?>
		<?php echo $form->dropDownList($model,'tipologia_id',$model->getListadoTipos(), array('empty'=>'-- Selecciona Tipologia --')); ?>
		<?php echo $form->error($model,'tipologia_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'municipio_id'); ?>
		<?php echo Chosen::activeDropDownList($model, 'municipio_id', CHtml::listData(Municipio::model()->findAll(),'id','nombre')); ?>
		<?php echo $form->error($model,'municipio_id'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->