<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta name="language" content="es">

	<!-- Favicone Icon -->
    <link rel="shortcut icon" type="image/x-icon" href="<?php echo Yii::app()->request->baseUrl; ?>/images/favicon.ico">
    <link rel="icon" type="image/png" href="<?php echo Yii::app()->request->baseUrl; ?>/images/favicon.png">
    <link rel="apple-touch-icon" href="<?php echo Yii::app()->request->baseUrl; ?>/images/favicon.png">
	
	<!-- CSS -->
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/bootstrap.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/font-awesome.css" rel="stylesheet" type="text/css" />

	<!-- blueprint CSS framework 
	<link rel="stylesheet" type="text/css" href="<?php //echo Yii::app()->request->baseUrl; ?>/css/screen.css" media="screen, projection">-->
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/print.css" media="print">
	
	<!-- Plugins -->
	<link href="<?php echo Yii::app()->request->baseUrl; ?>/css/simpletextrotator.css" rel="stylesheet">
	<link href="<?php echo Yii::app()->request->baseUrl; ?>/css/font-awesome.min.css" rel="stylesheet">
	<link href="<?php echo Yii::app()->request->baseUrl; ?>/css/et-line-font.css" rel="stylesheet">
	<link href="<?php echo Yii::app()->request->baseUrl; ?>/css/magnific-popup.css" rel="stylesheet">
	<link href="<?php echo Yii::app()->request->baseUrl; ?>/css/flexslider.css" rel="stylesheet">
	<link href="<?php echo Yii::app()->request->baseUrl; ?>/css/animate.css" rel="stylesheet">

	<!-- Template core CSS -->
	<link href="<?php echo Yii::app()->request->baseUrl; ?>/css/style.css" rel="stylesheet">
	
	<!-- Custom css -->
	<link href="<?php echo Yii::app()->request->baseUrl; ?>/css/custom.css" rel="stylesheet">

	<!--[if lt IE 8]>
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie.css" media="screen, projection">
	<![endif]-->

	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css">
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/form.css">

	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
</head>

<body>

	<!-- PRELOADER -->
	<div class="page-loader">
		<div class="loader">Cargando...</div>
	</div>
	<!-- /PRELOADER -->

<!-- NAVIGATION -->
	<nav class="navbar navbar-custom navbar-transparent navbar-fixed-top" role="navigation">

			<div class="container">
		
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#custom-collapse">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a href="<?php echo $this->createUrl('site/index'); ?>">
						<img src="<?php echo Yii::app()->request->baseUrl; ?>/images/logo.png" class="img-responsive" />
					</a>
				</div>
		
				<div class="collapse navbar-collapse" id="custom-collapse">
		
					<ul class="nav navbar-nav navbar-right">
		
						<li><a href="<?php echo $this->createUrl('site/index'); ?>">Inicio</a></li>

						<li><a href="<?php echo $this->createUrl('site/page', array('view'=>'sobre-mi')); ?>">Sobre mí</a></li>
		
						<li><a href="<?php echo $this->createUrl('site/proyectos'); ?>">Proyectos</a></li>
		
						<li><a href="<?php echo $this->createUrl('site/contact'); ?>">Contacto</a></li>

		
					</ul>
				</div>
		
			</div>

	</nav>
	<!-- /NAVIGATION -->
	


	
		
		<?php echo $content; ?>

		<!-- CONTACT -->
		<section class="module-small">
		
			<div class="container">
		
				<div class="row">
		
					<!-- CONTENT BOX -->
					<div class="col-sm-4">
						<div class="content-box">
							<div class="content-box-icon">
								<span class="icon-map-pin"></span>
							</div>
							<div class="content-box-title font-inc">
								Vall d'Alba, Castellón, Comunidad Valenciana
							</div>
						</div>
					</div>
					<!-- /CONTENT BOX -->
		
					<!-- CONTENT BOX -->
					<div class="col-sm-4">
						<div class="content-box">
							<div class="content-box-icon">
								<span class="icon-phone"></span>
							</div>
							<div class="content-box-title font-inc">
								684 05 36 76
							</div>
						</div>
					</div>
					<!-- /CONTENT BOX -->
		
					<!-- CONTENT BOX -->
					<div class="col-sm-4">
						<div class="content-box">
							<div class="content-box-icon">
								<span class="icon-envelope"></span>
							</div>
							<div class="content-box-title font-inc">
								info@essenciadisseny.es
							</div>
						</div>
					</div>
					<!-- /CONTENT BOX -->
		
				</div>
		
			</div>
		
		</section>
		<!-- /CONTACT -->

		<!-- FOOTER -->
		<footer class="footer">
		
			<div class="container">
		
				<div class="row">
		
					<div class="col-sm-12 text-center">
						<p class="copyright font-inc m-b-0">Essència Disseny | Diseño web, gráfico y fotografía en Castellón &copy; 2015-<?php echo date('Y'); ?></p>
					</div>
		
				</div>
		
			</div>
		
		</footer>
		<!-- /FOOTER -->

	</div>
	<!-- /WRAPPER -->


	<!-- Scroll-up -->
	<div class="scroll-up">
		<a href="#totop"><i class="fa fa-angle-double-up"></i></a>
	</div>

	<!-- JS -->
	<script src="<?php Yii::app()->clientScript->registerCoreScript('jquery');?>" type="text/javascript"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.mb.YTPlayer.min.js"></script>
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/appear.js"></script>
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.simple-text-rotator.min.js"></script>
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jqBootstrapValidation.js"></script>
	<script src="http://maps.google.com/maps/api/js?sensor=true"></script>
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/gmaps.js"></script>
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/isotope.pkgd.min.js"></script>
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/imagesloaded.pkgd.js"></script>
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.flexslider-min.js"></script>
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.magnific-popup.min.js"></script>
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.fitvids.js"></script>
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/smoothscroll.js"></script>
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/wow.min.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/contact.js"></script>
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/custom.js"></script>

</body>
</html>
