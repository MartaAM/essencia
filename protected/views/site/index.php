
	<!-- HOME -->
	<section id="home" class="module-hero module-parallax module-fade module-full-height bg-dark-30" data-background="<?php echo Yii::app()->request->baseUrl; ?>/images/fondo-1.png">

		<div class="hs-caption container">
			<div class="caption-content">
				<div class="hs-title-size-4 font-alt m-b-30">
					Nos gusta trabajar para hacer realidad tus ideas
				</div>
				<div class="hs-title-size-1 font-inc">
					Ponemos nuestra experiencia en diseño web, diseño gráfico y fotografía a su servicio.
				</div>
			</div>
		</div>

	</section >
	<!-- /HOME -->

	<!-- WRAPPER -->
	<div class="wrapper">


		<h1>Welcome to <i><?php echo CHtml::encode(Yii::app()->name); ?></i></h1>

		<p>Congratulations! You have successfully created your Yii application.</p>

		<p>You may change the content of this page by modifying the following two files:</p>
		<ul>
			<li>View file: <code><?php echo __FILE__; ?></code></li>
			<li>Layout file: <code><?php echo $this->getLayoutFile('main'); ?></code></li>
		</ul>

		<p>For more details on how to further develop this application, please read
		the <a href="http://www.yiiframework.com/doc/">documentation</a>.
		Feel free to ask in the <a href="http://www.yiiframework.com/forum/">forum</a>,
		should you have any questions.</p>


	