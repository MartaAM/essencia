<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name . ' - About';
$this->breadcrumbs=array(
	'About',
);
?>
<!-- WRAPPER -->
	<div class="wrapper">

		<!-- HOME -->
		<section class="module module-header bg-dark bg-dark-50" data-background="<?php echo Yii::app()->request->baseUrl; ?>/images/foto_sobre_mi3.jpg">

			<div class="container">

				<!-- MODULE TITLE -->
				<div class="row">

					<div class="col-sm-6 col-sm-offset-3">

						<h1 class="module-title font-alt align-center">Marta Agut</h1>

						<div class="module-subtitle font-inc align-center">
							A wonderful serenity has taken possession of my entire soul, like these sweet mornings of spring which I enjoy with my whole heart.
						</div>

					</div>

				</div>
				<!-- /MODULE TITLE -->

			</div>

		</section >
		<!-- /HOME -->

		<!-- ABOUT -->
		<section class="module">

			<div class="container">

				<!-- MODULE TITLE -->
				<div class="row">

					<div class="col-sm-6">

						<h2 class="module-title font-alt">Sobre mi</h2>

						<div class="module-subtitle font-inc">
							Cuando se une pasión y trabajo, solo pueden salir cosas extraordinarias. Así son nuestros proyectos.
						</div>

					</div>

				</div>
				<!-- /MODULE TITLE -->

				<div class="row">

					<!-- ABOUT CONTENT -->
					<div class="col-sm-6">

						<p>Hola! Mi nombre es Marta, desde muy pequeña me encantaba dibujar, cuando ya fui un poco más mayor me compraron mi primer ordenador, de esos que todavía tenían MS-DOS (Que tiempos aquellos!!) y ahí fue donde descubrí mi segunda pasión...la informática.</p>
			           <p>Y después de unos años he decidido dar un giro radical a mi vida y emprender con lo que más me gusta que es el diseño y el desarrollo web, así que deseo ser de ayuda en tus próximos proyectos!!</p>
			           <p>Mi idea es poder proporcionarte un servicio integral...desde tu imagen corporativa hasta tu página web, sin tener que contactar con 3 personas diferentes. Puesto que lo que menos necesitas cuando estás montando tu negocio son más trabas y dolores de cabeza. YO PUEDO AYUDARTE!!</p>

						<ul class="social-list">
							<li><a href="https://www.facebook.com/EssenciaDisseny/"><span class="icon-facebook"></span></a></li>
							<li><a href="https://twitter.com/Marta_Essencia"><span class="icon-twitter"></span></a></li>
							<li><a href="https://plus.google.com/+Ess%C3%A8nciaDissenyValldAlba"><span class="icon-googleplus"></span></a></li>
							<li><a href="https://www.instagram.com/essencia_disseny/"><span class="icon-aperture"></span></a></li>
						</ul>

					</div>
					<!-- /ABOUT CONTENT -->

					<!-- SKILLS -->
					<div class="col-sm-6">

						<h5 class="progress-title font-inc">Diseño Web</h5>
						<div class="progress">
							<div class="progress-bar pb-dark" aria-valuenow="85" role="progressbar" aria-valuemin="0" aria-valuemax="100">
								<span class="font-inc"></span>
							</div>
						</div>

						<h5 class="progress-title font-inc">Diseño Gráfico</h5>
						<div class="progress">
							<div class="progress-bar pb-dark" aria-valuenow="90" role="progressbar" aria-valuemin="0" aria-valuemax="100">
								<span class="font-inc"></span>
							</div>
						</div>

						<h5 class="progress-title font-inc">Marketing</h5>
						<div class="progress">
							<div class="progress-bar pb-dark" aria-valuenow="50" role="progressbar" aria-valuemin="0" aria-valuemax="100">
								<span class="font-inc"></span>
							</div>
						</div>

						<h5 class="progress-title font-inc">Fotografía</h5>
						<div class="progress">
							<div class="progress-bar pb-dark" aria-valuenow="90" role="progressbar" aria-valuemin="0" aria-valuemax="100">
								<span class="font-inc"></span>
							</div>
						</div>

					</div>
					<!-- /SKILLS -->

				</div>

			</div>

		</section >
		<!-- /ABOUT -->

