<?php
/* @var $this SiteController */
/* @var $model ContactForm */
/* @var $form CActiveForm */

$this->pageTitle=Yii::app()->name . ' - Contact Us';
$this->breadcrumbs=array(
	'Contact',
);
?>
	<!-- GOOGLE MAP -->
	<section id="map-section" class="module-hero module-parallax module-fade">
		<div id="map"></div>
	</section>
	<!-- /GOOGLE MAP -->

	<!-- WRAPPER -->
	<div class="wrapper">

<?php if(Yii::app()->user->hasFlash('contact')): ?>

<div class="flash-success">
	<?php echo Yii::app()->user->getFlash('contact'); ?>
</div>

<?php else: ?>

		<!-- CONTACT -->
		<section class="module">

			<div class="container">

				<!-- MODULE TITLE -->
				<div class="row">

					<div class="col-sm-6 col-sm-offset-3">

						<h2 class="module-title align-center font-alt">Contacto</h2>

						<div class="module-subtitle align-center font-inc">
							Si tienes cualquier pregunta, por favor rellena el siguiente formulario para ponerte en contacto con nosotros. Gracias.
						</div>

					</div>

				</div>
				<!-- /MODULE TITLE -->

				<!-- CONTACT FORM -->
				<div class="row">

					<div class="col-sm-6 col-sm-offset-3">

						<?php $form=$this->beginWidget('CActiveForm', array(
								'id'=>'contact-form',
								'enableClientValidation'=>true,
								'clientOptions'=>array(
									'validateOnSubmit'=>true,
								),
                                'htmlOptions'=>array(
                                    'class'=>'contact-form',
                                    'role'=>'form'
                                ),
							)); ?>

							<div class="form-group">
								<?php echo $form->labelEx($model,'name', array('class'=>'sr-only')); ?>
                                <?php echo $form->textField($model,'name', array('class'=>'form-control', 'placeholder'=>$model->getAttributeLabel('name') )); ?>
                                <?php echo $form->error($model,'name'); ?>
							</div>

							<div class="form-group">
								 <?php echo $form->labelEx($model,'email', array('class'=>'sr-only')); ?>
                                <?php echo $form->textField($model,'email', array('class'=>'form-control', 'placeholder'=>$model->getAttributeLabel('email') )); ?>
                                <?php echo $form->error($model,'email'); ?>
							</div>

							<div class="form-group">
								<?php echo $form->labelEx($model,'subject', array('class'=>'sr-only')); ?>
                                <?php echo $form->textField($model,'subject', array('class'=>'form-control', 'placeholder'=>$model->getAttributeLabel('subject') )); ?>
                                <?php echo $form->error($model,'subject'); ?>
							</div>

							<div class="form-group">
								<?php echo $form->labelEx($model,'body', array('class'=>'sr-only')); ?>
                                <?php echo $form->textArea($model,'body', array('class'=>'form-control','placeholder'=>$model->getAttributeLabel('body'), 'rows'=>6 )); ?>
                                <?php echo $form->error($model,'body'); ?>
							</div>

							<button type="submit" class="btn btn-round btn-g">Enviar</button>

						</form>

						<!-- Ajax response -->
						<div id="contact-response" class="ajax-response font-alt"></div>
						<?php $this->endWidget(); ?>
					</div>

				</div>
				<!-- /CONTACT FORM -->

			</div>

		</section>
		<!-- /CONTACT -->
<?php endif; ?>
		<!-- DIVIDER -->
		<hr class="divider-w">
		<!-- /DIVIDER -->

