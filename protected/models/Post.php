<?php

/**
 * This is the model class for table "post".
 *
 * The followings are the available columns in table 'post':
 * @property integer $id
 * @property string $titulo
 * @property string $contenido
 * @property string $status
 * @property string $tags
 * @property integer $autor_id
 *
 * The followings are the available model relations:
 * @property Comentario[] $comentarios
 * @property Usuario $autor
 */
class Post extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */

	const STATUS_DRAFT=1;
	const STATUS_PUBLISHED=2;
	const STATUS_ARCHIVED=3;

	private $_oldTags;

	public function tableName()
	{
		return 'post';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('titulo, contenido, status', 'required'),
			array('autor_id', 'numerical', 'integerOnly'=>true),
			array('titulo', 'length', 'max'=>128),
			array('status','in','range'=>array(1,2,3)),
			array('tags','match','pattern'=>'/^[\w\s,]+$/','message'=>'Tags solo pueden contener caracteres alfabéticos.'),
			array('tags', 'normalizeTags'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('titulo, status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'autor' => array(self::BELONGS_TO, 'Usuario', 'autor_id'),
			'comentarios' => array(self::HAS_MANY, 'Comentario', 'post_id','condition'=>'comentarios.status='.Comentario::STATUS_APPROVED,'order'=>'comentarios.create_time DESC'),
			'comentarioCount' => array(self::STAT, 'Comentario', 'post_id','condition'=>'status='.Comentario::STATUS_APPROVED),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'titulo' => 'Titulo',
			'contenido' => 'Contenido',
			'status' => 'Status',
			'create_time'=>'Fecha Creación',
			'update_time'=>'Fecha Actualización',
			'tags' => 'Tags',
			'autor_id' => 'Autor',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		$criteria=new CDbCriteria;

		$criteria->compare('titulo',$this->titulo,true);

		$criteria->compare('status',$this->status);

		return new CActiveDataProvider('Post', array(
			'criteria'=>$criteria,
			'sort'=>array(
				'defaultOrder'=>'status, update_time DESC',
			),
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Post the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function getUrl(){
		return Yii::app()->createUrl('post/view', array(
			'id'=>$this->id,
			'titulo'=>$this->titulo,
		));
	}

	public function getTagLinks()
	{
		$links=array();
		foreach(Tag::string2array($this->tags) as $tag)
			$links[]=CHtml::link(CHtml::encode($tag), array('post/index', 'tag'=>$tag));
		return $links;
	}

	public function normalizeTags($attributes,$params){
		$this->tags = Tag::array2string(array_unique(Tag::string2array($this->tags)));
	}

	protected function beforeSave(){
		if(parent::beforeSave()){
			if($this->isNewRecord){
				$this->create_time=$this->update_time=time();
				$this->autor_id=Yii::app()->user->id;
			}
			else
				$this->update_time=time();
			return true;
		}
		else
			return false;
	}


	protected function afterSave(){
		parent::afterSave();
		Tag::model()->updateFrecuencia($this->_oldTags, $this->tags);
	}

	protected function afterFind(){
		parent::afterFind();
		$this->_oldTags=$this->tags;
	}

	protected function afterDelete(){
		parent::afterDelete();
		Comentario::model()->deleteAll('post_id='.$this->id);
		Tag::model()->updateFrecuencia($this->tags, '');
	}

	public function addComentario($comentario)
	{
		if(Yii::app()->params['commentNeedApproval'])
			$comentario->status=Comentario::STATUS_PENDING;
		else
			$comentario->status=Comentario::STATUS_APPROVED;
		$comentario->post_id=$this->id;
		return $comentario->save();
	}
}
