<?php

/**
 * This is the model class for table "comentario".
 *
 * The followings are the available columns in table 'comentario':
 * @property integer $id
 * @property string $contenido
 * @property string $fecha_creacion
 * @property string $autor
 * @property string $email
 * @property string $status
 * @property string $url
 * @property integer $post_id
 *
 * The followings are the available model relations:
 * @property Post $post
 */
class Comentario extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */

	const STATUS_PENDING=1;
	const STATUS_APPROVED=2;

	public function tableName()
	{
		return 'comentario';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('contenido, autor, email', 'required'),
			array('post_id', 'numerical', 'integerOnly'=>true),
			array('autor, email, url', 'length', 'max'=>128),
			array('email','email'),
			array('url','url'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, contenido, fecha_creacion, autor, email, status, url, post_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'post' => array(self::BELONGS_TO, 'Post', 'post_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'Id',
			'contenido' => 'Comentario',
			'fecha_creacion' => 'Fecha Creación',
			'autor' => 'Autor',
			'email' => 'Email',
			'status' => 'Status',
			'url' => 'Web',
			'post_id' => 'Post',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('contenido',$this->contenido,true);
		$criteria->compare('fecha_creacion',$this->fecha_creacion,true);
		$criteria->compare('autor',$this->autor,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('status',$this->status,true);
		$criteria->compare('url',$this->url,true);
		$criteria->compare('post_id',$this->post_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Comentario the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function approve()
	{
		$this->status=Comentario::STATUS_APPROVED;
		$this->update(array('status'));
	}

	protected function beforeSave()
	{
		if(parent::beforeSave())
		{
			if($this->isNewRecord)
				$this->fecha_creacion=time();
			return true;
		}
		else
			return false;
	}

	public function getUrl($post=null)
	{
		if($post===null)
			$post=$this->post;
		return $post->url.'#c'.$this->id;
	}

	public function getAutorLink()
	{
		if(!empty($this->url))
			return CHtml::link(CHtml::encode($this->autor),$this->url);
		else
			return CHtml::encode($this->autor);
	}

	public function getPendingComentarioCount()
	{
		return $this->count('status='.self::STATUS_PENDING);
	}

	public function findRecentComentarios($limit=10)
	{
		return $this->with('post')->findAll(array(
			'condition'=>'t.status='.self::STATUS_APPROVED,
			'order'=>'t.fecha_creacion DESC',
			'limit'=>$limit,
		));
	}
}
