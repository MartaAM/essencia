<?php

/**
 * This is the model class for table "proyecto".
 *
 * The followings are the available columns in table 'proyecto':
 * @property integer $id
 * @property string $nombre
 * @property string $descripcion
 * @property string $slug
 * @property string $url
 * @property string $tecnologia
 * @property integer $tipologia_id
 * @property integer $municipio_id
 *
 * The followings are the available model relations:
 * @property Municipio $municipio
 * @property Tipologia $tipologia
 */
class Proyecto extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'proyecto';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nombre, descripcion, slug, tipologia_id, municipio_id', 'required'),
			array('tipologia_id, municipio_id', 'numerical', 'integerOnly'=>true),
			array('nombre, slug, url', 'length', 'max'=>255),
			array('tecnologia', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, nombre, descripcion, slug, url, tecnologia, tipologia_id, municipio_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'municipio' => array(self::BELONGS_TO, 'Municipio', 'municipio_id'),
			'tipologia' => array(self::BELONGS_TO, 'Tipologia', 'tipologia_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'nombre' => 'Nombre',
			'descripcion' => 'Descripcion',
			'slug' => 'Slug',
			'url' => 'Url',
			'tecnologia' => 'Tecnologia',
			'tipologia_id' => 'Tipologia',
			'municipio_id' => 'Municipio',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('nombre',$this->nombre,true);
		$criteria->compare('descripcion',$this->descripcion,true);
		$criteria->compare('slug',$this->slug,true);
		$criteria->compare('url',$this->url,true);
		$criteria->compare('tecnologia',$this->tecnologia,true);
		$criteria->compare('tipologia_id',$this->tipologia_id);
		$criteria->compare('municipio_id',$this->municipio_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Proyecto the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function getListadoTipos(){
        $tipologia=Tipologia::model()->findAll();
        return CHtml::listData($tipologia,"id","nombre");
    }

    public function behaviors()
    {
        return array(
            'sluggable'=>array(
                'class'=>'ext.RSluggable',
                'atributo'=>'slug',
                'valor'=>'nombre'
            ),

            'galleryBehavior'=>array(
                'class'=>'GalleryBehavior',
                'idAttribute'=>'gallery_id',
                'versions'=>array(
                	'detalle'=>array(
                		'resize' =>array(300,NULL)
                	),
                    'completo'=>array(
                    	'resize'=>array(1150, NULL)
                    )
                ),
                'name'=>TRUE,
                'description'=>TRUE
            )
        );
    }
}
