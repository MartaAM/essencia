<?php

class m160511_184053_create_comentario extends CDbMigration
{
	public function up()
	{

		// Aqui creas las tablass
		$this->createTable('comentario', array(
			'id'=>'pk',
			'contenido'=>'string NOT NULL',
			'fecha_creacion'=>'date NOT NULL',
			'autor'=>'string NOT NULL',
			'email'=>'string NOT NULL',
			'status'=>'string NOT NULL',
			'url'=>'string',
			'post_id'=>'int NOT NULL'
			));

		// Despues las relaciones
		$this->addForeignKey('fk_coment_post','comentario','post_id','post','id','CASCADE','CASCADE');

	}

	public function down()
	{
		$this->dropTable('comentario');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}