<?php

class m160503_105353_create_tipologia extends CDbMigration
{
	public function up()
	{

		// Aqui creas las tablass
		$this->createTable('tipologia', array(
			'id'=>'pk',
			'nombre'=>'string NOT NULL'
			));

	}

	public function down()
	{
		$this->dropTable('tipologia');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}