<?php

class m160511_182432_create_usuario extends CDbMigration
{
	public function up()
	{
		// Aqui creas las tablass
		$this->createTable('usuario', array(
			'id'=>'pk',
			'nombre'=>'string NOT NULL',
			'password'=>'string NOT NULL',
			'email'=>'string NOT NULL'
			));


		// Despues las relaciones
	}

	public function down()
	{
		$this->dropTable('usuario');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}