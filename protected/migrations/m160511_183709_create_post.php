<?php

class m160511_183709_create_post extends CDbMigration
{
	public function up()
	{

		// Aqui creas las tablass
		$this->createTable('post', array(
			'id'=>'pk',
			'titulo'=>'string NOT NULL',
			'contenido'=>'string NOT NULL',
			'status'=>'string NOT NULL',
			'tags'=>'string NOT NULL',
			'autor_id'=>'int NOT NULL',
			'create_time'=>'time'
			));

		// Despues las relaciones
		$this->addForeignKey('fk_post_usuario','post','autor_id','usuario','id','CASCADE','CASCADE');

	}

	public function down()
	{
		$this->dropTable('post');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}