<?php

class m160511_184822_create_tag extends CDbMigration
{
	public function up()
	{

		// Aqui creas las tablass
		$this->createTable('tag', array(
			'id'=>'pk',
			'nombre'=>'string NOT NULL',
			'frecuencia'=>'int NOT NULL'
			));

		// Despues las relaciones

	}

	public function down()
	{
		$this->dropTable('tag');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}