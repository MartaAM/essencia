<?php

class m160503_101141_create_municipio extends CDbMigration
{
	public function up()
	{

		// Aqui creas las tablass
		$this->createTable('municipio', array(
			'id'=>'pk',
			'nombre'=>'string NOT NULL',
						'provincia_id'=>'int not null'
			));

		// Despues las relaciones
		$this->addForeignKey('fk_mun_prov','municipio','provincia_id','provincia','id','CASCADE','CASCADE');

	}

	public function down()
	{
		$this->dropForeignKey('fk_mun_prov','municipio');
		$this->dropTable('municipio');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}