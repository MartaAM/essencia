<?php

class m160503_105552_create_proyecto extends CDbMigration
{
	public function up()
	{

		// Aqui creas las tablass
		$this->createTable('proyecto', array(
			'id'=>'pk',
			'nombre'=>'string NOT NULL',
			'descripcion'=>'text NOT NULL',
			'slug'=>'string NOT NULL',
			'url'=>'string',
			'tecnologia'=>'text',
			'tipologia_id' => 'int not null',
			'municipio_id'=>'int not null'
			));

		// Despues las relaciones
		$this->addForeignKey('fk_pro_tipo','proyecto','tipologia_id','tipologia','id','CASCADE','CASCADE');
		$this->addForeignKey('fk_pro_mun','proyecto','municipio_id','municipio','id','CASCADE','CASCADE');

	}

	public function down()
	{
		$this->dropForeignKey('fk_pro_tipo','proyecto');
		$this->dropForeignKey('fk_pro_num','proyecto');
		$this->dropTable('proyecto');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}