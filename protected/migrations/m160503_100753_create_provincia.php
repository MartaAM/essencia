<?php

class m160503_100753_create_provincia extends CDbMigration
{
	public function up()
	{

		// Aqui creas las tablass
		$this->createTable('provincia', array(
			'id'=>'pk',
			'nombre'=>'string NOT NULL',
			'comunidad_id'=>'int not null'
			));

		// Despues las relaciones
		$this->addForeignKey('fk_prov_com','provincia','comunidad_id','comunidad','id','CASCADE','CASCADE');

	}

	public function down()
	{
		$this->dropForeignKey('fk_prov_com','provincia');
		$this->dropTable('provincia');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}