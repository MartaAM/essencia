
<?php

class m160503_094823_create_comunidad extends CDbMigration
{
	public function up()
	{
		// Aqui creas las tablass
		$this->createTable('comunidad', array(
			'id'=>'pk',
			'nombre'=>'string NOT NULL'
			));


		// Despues las relaciones
	}

	public function down()
	{
		$this->dropTable('comunidad');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}