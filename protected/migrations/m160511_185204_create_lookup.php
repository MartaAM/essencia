<?php

class m160511_185204_create_lookup extends CDbMigration
{
	public function up()
	{

		// Aqui creas las tablass
		$this->createTable('lookup', array(
			'id'=>'pk',
			'nombre'=>'string',
			'codigo'=>'int',
			'tipo'=>'string',
			'posicion'=>'string'
			));

		// Despues las relaciones

	}

	public function down()
	{
		$this->dropTable('lookup');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}